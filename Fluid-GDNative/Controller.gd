extends Node2D

var fluid

var water = false
var wall = false
var remove = false
# Called when the node enters the scene tree for the first time.
func _ready():
	fluid = get_node("Sprite")


func addWater():
	var position = get_viewport().get_mouse_position()
	var size = fluid.get("spriteSize")
	if(position.x<size.x and position.x>0 and position.y<size.y and position.y>0 ):
		fluid.addWater(Vector2(position.x, position.y), size.x+size.y)
		
		
func addWall():
	var position = get_viewport().get_mouse_position()
	var size = fluid.get("spriteSize")
	var wallSize = Vector2(size.x,size.x) * fluid.get("borderSize")/2
	
	for x in range(position.x-wallSize.x, position.x+wallSize.x):
		for y in range(position.y-wallSize.y, position.y+wallSize.y):
			if(x<size.x and x>0 and y<size.y and y>0 ):
				fluid.addWall(Vector2(x, y))
				
func removeWall():
	var position = get_viewport().get_mouse_position()
	var size = fluid.get("spriteSize")
	var wallSize = Vector2(size.x,size.x) * fluid.get("borderSize")/2
	
	for x in range(position.x-wallSize.x, position.x+wallSize.x):
		for y in range(position.y-wallSize.y, position.y+wallSize.y):
			if(x<size.x and x>0 and y<size.y and y>0 ):
				fluid.removeWall(Vector2(x, y))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(water):  addWater()
	if(wall):
		if(remove): removeWall()
		else: addWall()


func _input(event):
	if(event is InputEventMouseButton ):
		if(event.is_action_pressed("water")): water = true
		if(event.is_action_released("water")): water = false
		if(event.is_action_pressed("wall")): wall = true
		if(event.is_action_released("wall")): wall = false
		if(event.is_action_pressed("Remove")): remove = not remove
