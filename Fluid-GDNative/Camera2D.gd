extends Camera2D


var sprite
func _ready():
	sprite = get_parent().get_parent().get_node("Sprite")
	var size = sprite.get("spriteSize")
	
	if(size.x> size.y): get_viewport().set_size_override(true, Vector2(size.x, size.x))
	else:  get_viewport().set_size_override(true, Vector2(size.y, size.y))
	get_viewport().set_size_override_stretch(true) # Enable stretch for custom size.
	sprite.position = size-size/2
	
		
func _process(delta):
	print("FPS: " + str(Engine.get_frames_per_second()))
	pass
