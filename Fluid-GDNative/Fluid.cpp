#include "Fluid.h"

void Fluid::_register_methods()
{
	register_method("_ready", &Fluid::_ready);
	register_method("_process", &Fluid::_process);
	register_method("_physics_process", &Fluid::_physics_process);
	register_method("addWall", &Fluid::addWall);
	register_method("removeWall", &Fluid::delWall);
	register_method("addWater", &Fluid::addWater);

	register_property("spriteSize", &Fluid::spriteSize, Vector2());
	register_property("maxMass", &Fluid::max_mass, 1.0f);
	register_property("minMass", &Fluid::min_mass, 0.005f);
	register_property("maxCompress", &Fluid::max_compress, 0.25f);
	register_property("minFlowSpeed", &Fluid::min_flow, 0.005f);
	register_property("normalFlowSpeed", &Fluid::flow_speed, 1.0f);
	register_property("maxFlowSpeed", &Fluid::max_flow_speed, 4.0f);
	register_property("borderSize", &Fluid::border_thickness, 0.1f);
	register_property("colorChange", &Fluid::color_change, 0.005f);
	register_property("WaterAmount", &Fluid::water_amount_per_frame, 1.000f);


}

void Fluid::_init()
{
	color_change = 0.005f;
	border_thickness = 0.1f;
	spriteSize = Vector2(100, 100);
	max_mass = 1.0f;
	min_mass = 0.005f;

	max_compress = 0.25f;


	default_wall_mass = -666.0f;


	min_flow = 0.005f;
	flow_speed = 1.0f;
	max_flow_speed = 4.0f;

	air = Color(0.75, 0.75, 0.75, 1);
	wall = Color(0.87, 0.72, 0.53, 1);
	water = Color(0.5, 1, 0.83, 1);

	water_amount_per_frame = 1.0f;
}

int Fluid::m2a(int x, int y)
{
	return x + y * int(spriteSize.x);
}

float Fluid::StabilizeFluidBelow(float total_mass)
{
	if (total_mass <= max_mass) return max_mass;
	else if (total_mass < 2 * max_mass + max_compress)
		return (max_mass * max_mass + total_mass * max_compress) / (max_mass + max_compress);
	else return (total_mass + max_compress) / 2;
}

void Fluid::UpdateCell(int x, int y)
{
	float flow = 0.f;
	float remaining_mass = matrix[m2a(x,y)];
	if (remaining_mass == default_wall_mass) return;
	else if (remaining_mass < 0) new_matrix[m2a(x, y)] = 0.0f;

	if (remaining_mass <= min_mass)
	{
		new_matrix[x + y * int(spriteSize.x)] = 0.0;
		return;
	}

#pragma region Propagate Below
	if (y + 1 > spriteSize.y)
	{
		matrix[x + y * int(spriteSize.x)] = 0.0;
		return;
	}

	if (matrix[m2a(x, y+1)] != default_wall_mass) {
		float flow = StabilizeFluidBelow(remaining_mass + matrix[m2a(x, y+1)]);
		if (flow > min_flow) flow *= flow_speed;
		flow = Constrain(flow, 0, Min(max_flow_speed, remaining_mass));

		new_matrix[m2a(x, y)] -= flow;
		new_matrix[m2a(x, y+1)] += flow;
		remaining_mass -= flow;
	}
#pragma endregion

	if (remaining_mass <= 0) return;

#pragma region Propagat Left

	if (x - 1 >= 0 && matrix[m2a(x-1, y)] != default_wall_mass) {
		float flow = (matrix[m2a(x, y)] - matrix[m2a(x-1, y)]) / 4;
		if (flow > min_flow) flow *= flow_speed;
		flow = Constrain(flow, 0, remaining_mass);
		new_matrix[m2a(x, y)] -= flow;
		new_matrix[m2a(x-1, y)] += flow;
		remaining_mass -= flow;
	}
#pragma endregion

	if (remaining_mass <= 0) return;

#pragma region Propagate Right
	if (x + 1 < spriteSize.x && matrix[m2a(x+1, y)] != default_wall_mass) {
		flow = (matrix[m2a(x, y)] - matrix[m2a(x+1, y)]) / 4;
		if (flow > min_flow) flow *= flow_speed;
		flow = Constrain(flow, 0, remaining_mass);

		new_matrix[m2a(x, y)] -= flow;
		new_matrix[m2a(x+1, y)] += flow;
		remaining_mass -= flow;
	}
#pragma endregion

	if (remaining_mass <= 0) return;

#pragma region Propagate Up
	if (y - 1 > 0 && matrix[m2a(x, y-1)] != default_wall_mass) {
		flow = remaining_mass - StabilizeFluidBelow(remaining_mass + matrix[m2a(x, y-1)]);
		if (flow > min_flow) flow *= flow_speed;
		flow = Constrain(flow, 0, remaining_mass);
		new_matrix[m2a(x, y)] -= flow;
		new_matrix[m2a(x, y-1)] += flow;
		remaining_mass -= flow;
	}
#pragma endregion


}

float Fluid::Constrain(float value, float Min, float max)
{
	if (value > Min && value < max) return value;
	else if (value < Min) return Min;
	else if (value > max) return max;
	else return value;
}

float Fluid::Min(float val1, float val2)
{
	if (val1 > val2) return val2;
	else return val1;
}

void Fluid::SimulateFlow()
{
	//for (int i = 0; i < spriteSize.x; i++)
	//	for (int j = 0; j < spriteSize.y; j++)
	for (int i = spriteSize.x-1; i >=0 ; i--)
		for (int j = spriteSize.y-1; j >=0; j--)
		{
			UpdateCell(i, j);
		}
	matrix = new_matrix;
}

void Fluid::createBorder(int thickness)
{
	for (int i = 0; i < thickness; i++)
	{
		for (int x = 0; x < spriteSize.x; x++)
		{
			matrix[m2a(x, 0 + i)] = default_wall_mass;
			matrix[m2a(x, spriteSize.y - 1 - i)] = default_wall_mass;
		}
		for (int y = 0; y < spriteSize.y; y++)
		{
			matrix[m2a(0 + i,y)] = default_wall_mass;
			matrix[m2a(spriteSize.x - 1 - i, y)] = default_wall_mass;
		}
	}
}

void Fluid::fillImage()
{
	img->fill(air);
	img->lock();

	for (int x = 0; x < spriteSize.x; x++)
		for (int y = 0; y < spriteSize.y; y++)
		{
			if (matrix[m2a(x, y)] == 0) continue;
			if (matrix[m2a(x, y)] == default_wall_mass) img->set_pixel(x, y, wall);
			else
			{
				float var = matrix[m2a(x, y)] * color_change;

				img->set_pixel(x, y, Color(
					water.r - water.r * var,
					water.g - water.g * var,
					water.b - water.b * var, 1));
			}


		}
	img->unlock();
}

void Fluid::image2Texture()
{
	ImageTexture* tex;
	tex = ImageTexture::_new();
	tex->set_storage(ImageTexture::STORAGE_RAW);
	tex->create_from_image(img);
	set_texture(tex);
}

void Fluid::addWall(Vector2 position)
{
	if(matrix[m2a(position.x, position.y)] != default_wall_mass) matrix[m2a(position.x, position.y)] = default_wall_mass;
}

void Fluid::delWall(Vector2 position)
{
	if (matrix[m2a(position.x, position.y)] == default_wall_mass) matrix[m2a(position.x, position.y)] = 0.0f;
}

void Fluid::addWater(Vector2 position, float mass)
{
	if (matrix[m2a(position.x, position.y)] == default_wall_mass) return;
	matrix[m2a(position.x, position.y)] = mass*water_amount_per_frame;
}


Fluid::Fluid()
{
}

void Fluid::_ready()
{
	img.instance();
	matrix = new float[int((spriteSize.x+1) * (spriteSize.y+1))];
	new_matrix = new float[int((spriteSize.x + 1) * (spriteSize.y + 1))];
	for (int i = 0; i < spriteSize.x; i++)
		for (int j = 0; j < spriteSize.y; j++)
		{
			matrix[m2a(i, j)] = 0.0f;
			new_matrix[m2a(i, j)] = 0.0f;
		}

	createBorder(int(spriteSize.x * border_thickness));
	img->create(spriteSize.x, spriteSize.y, false, Image::FORMAT_RGB8);
	fillImage();
	image2Texture();
	new_matrix = matrix;

}

void Fluid::_process(float delta)
{
	
	fillImage();
	image2Texture();
	
}

void Fluid::_physics_process(float delta)
{
	SimulateFlow();
}


float Fluid::Get_impassable_value()
{
	return default_wall_mass;
}
