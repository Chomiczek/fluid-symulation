#pragma once
#ifndef FLUID_H
#define FLUID_H

#include "common_headers.h"
#include <Sprite.hpp>
#include <Vector2.hpp>
#include <Color.hpp>
#include <Image.hpp>
#include <ImageTexture.hpp>
#include <InputEvent.hpp>
#include <Input.hpp>
#include <InputEventMouseButton.hpp>

class Fluid: public Sprite
{
	GODOT_CLASS(Fluid, Sprite);

public: 
	static void _register_methods();

	void _init();


protected:
	float water_amount_per_frame;
	float border_thickness;

	float max_mass;
	float min_mass;

	float max_compress;


	float default_wall_mass;


	float min_flow;
	float flow_speed;
	float max_flow_speed;

	float* matrix;
	float* new_matrix;
	

	Ref<Image> img;


public:
	Vector2 spriteSize;

	float color_change;

	Color air;
	Color water;
	Color wall;

protected:
	// Changes matrix 2D coordinates to 1D array coordinate 
	int m2a(int x, int y);

	float StabilizeFluidBelow(float total_mass);

	void UpdateCell(int i, int j);

	float Constrain(float value, float Min, float max);

	float Min(float val1, float val2);

	void SimulateFlow();

	
#pragma region Visualisation
	void createBorder(int thickness);
	void fillImage();
	void image2Texture();

	void addWall(Vector2 position);
	void delWall(Vector2 position);

	void addWater(Vector2 position, float mass);

#pragma endregion

	

	

public:
	Fluid();

	void _ready();
	void _process(float delta);
	void _physics_process(float delta);

	float Get_impassable_value();
};
#endif // FLUID_H
